package endian

import (
	"encoding/binary"
	"unsafe"
)

var swap16 func(uint16) uint16
var swap32 func(uint32) uint32
var swap64 func(uint64) uint64

var SystemByteOrder binary.ByteOrder

func init() {
	buf := [2]byte{}
	*(*uint16)(unsafe.Pointer(&buf[0])) = uint16(0xABCD)

	switch buf {
	case [2]byte{0xCD, 0xAB}:
		// little endian
		swap16 = _swap16
		swap32 = _swap32
		swap64 = _swap64
		SystemByteOrder = binary.LittleEndian
	case [2]byte{0xAB, 0xCD}:
		// big endian
		swap16 = _noop16
		swap32 = _noop32
		swap64 = _noop64
		SystemByteOrder = binary.BigEndian
	default:
		panic("Could not determine native endianness.")
	}
}

func _noop16(x uint16) uint16 {
	return x
}
func _noop32(x uint32) uint32 {
	return x
}
func _noop64(x uint64) uint64 {
	return x
}
func _swap16(x uint16) uint16 {
	return (x << 8) | (x >> 8)
}
func _swap32(x uint32) uint32 {
	x = ((x << 8) & 0xFF00FF00) | ((x >> 8) & 0xFF00FF)
	return (x << 16) | (x >> 16)
}
func _swap64(x uint64) uint64 {
	x = ((x << 8) & 0xFF00FF00FF00FF00) | ((x >> 8) & 0x00FF00FF00FF00FF)
	x = ((x << 16) & 0xFFFF0000FFFF0000) | ((x >> 16) & 0x0000FFFF0000FFFF)
	return (x << 32) | (x >> 32)
}

func Ntoh16(x uint16) uint16 {
	return swap16(x)
}
func Hton16(x uint16) uint16 {
	return swap16(x)
}

func Ntoh32(x uint32) uint32 {
	return swap32(x)
}
func Hton32(x uint32) uint32 {
	return swap32(x)
}

func Ntoh64(x uint64) uint64 {
	return swap64(x)
}
func Hton64(x uint64) uint64 {
	return swap64(x)
}
