package endian

import (
	"testing"

	"github.com/alecthomas/assert"
)

func TestSwap16(t *testing.T) {
	v := _swap16(0xABCD)
	assert.Equal(t, uint16(0xCDAB), v)
}
func TestSwap32(t *testing.T) {
	v := _swap32(0xABCD1234)
	assert.Equal(t, uint32(0x3412CDAB), v)
}
func TestSwap64(t *testing.T) {
	v := _swap64(0xABCD1234EF0155AA)
	assert.Equal(t, uint64(0xAA5501EF3412CDAB), v)
}

func TestNativeNtoh16(t *testing.T) {
	v := Ntoh16(0xABCD)
	t.Logf("0x%04X -> 0x%04X", 0xABCD, v)
}

func TestSystemByteOrder(t *testing.T) {
	v := SystemByteOrder.Uint16([]byte{0xAB, 0xCD})
	t.Logf("0x%04X -> 0x%04X", 0xABCD, v)
}
